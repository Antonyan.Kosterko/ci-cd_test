import unittest


def prime_divisors(n):
    divisors = []
    divisor = 2
    while n > 1:
        while n % divisor == 0:
            divisors.append(divisor)
            n //= divisor
        divisor += 1
    return divisors


class TestPrimeDivisors(unittest.TestCase):
    def test_prime_divisors(self):
        self.assertEqual(prime_divisors(1), [])
        self.assertEqual(prime_divisors(2), [2])
        self.assertEqual(prime_divisors(3), [3])
        self.assertEqual(prime_divisors(4), [2, 2])
        self.assertEqual(prime_divisors(5), [5])
        self.assertEqual(prime_divisors(6), [2, 3])
        self.assertEqual(prime_divisors(7), [7])
        self.assertEqual(prime_divisors(8), [2, 2, 2])
        self.assertEqual(prime_divisors(9), [3, 3])
        self.assertEqual(prime_divisors(10), [2, 5])
        self.assertEqual(prime_divisors(11), [11])
        self.assertEqual(prime_divisors(12), [2, 2, 3])
        self.assertEqual(prime_divisors(13), [13])
        self.assertEqual(prime_divisors(14), [2, 7])
        self.assertEqual(prime_divisors(15), [3, 5])
        self.assertEqual(prime_divisors(16), [2, 2, 2, 2])
        self.assertEqual(prime_divisors(17), [17])
        self.assertEqual(prime_divisors(18), [2, 3, 3])
        self.assertEqual(prime_divisors(19), [19])
        self.assertEqual(prime_divisors(20), [2, 2, 5])


if __name__ == '__main__':
    unittest.main()
